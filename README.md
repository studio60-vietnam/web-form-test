# Web Form Test #

Please create a simple web application with these following requirements:

* An age gate to verify the user must be 18 or older to progress to the next page.

* The next page will be a categories and products listing page, maybe some alcohol or beers. Product data will be loaded from an Xml.
Basically we will need a list of categories showing with Repeater, under each category will be a list of products, can be Repeater, Grid or any control.

* A couple sorting functions on product listing page such as Highest to lowest price, Lowest to highest price